<?php


use Phinx\Migration\AbstractMigration;

class AreaTable extends AbstractMigration
{
    public function up(){
    	$area = $this->table("Area");
    	$area->addColumn("shortName", "string")->addColumn("longName", "string")->create();
	}

	public function down(){
    	$this->table("Area")->drop();
	}
}
