<?php


use Phinx\Migration\AbstractMigration;

class CompanyTable extends AbstractMigration {

    public function up(){
    	$company = $this->table("Company");
    	$company->addColumn("name","string")->addColumn("brand", "json")->create();
	}

	public function down(){
    	$this->table("Company")->drop();
	}
}
