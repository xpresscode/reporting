<?php


use Phinx\Migration\AbstractMigration;

class ReportTable extends AbstractMigration
{
    public function up(){
    	$report = $this->table("Report");
    	$report->addColumn("companyId","integer")->addColumn("productName", "string")
			->addColumn("year", "integer")->addColumn("month", "integer")
			->addColumn("day", "integer")->addColumn("trackId","integer")
			->addColumn("raceNumber", "integer")->addColumn("postTime", "time")
			->addColumn("areaId", "integer")->addColumn("currencyId", "integer")
			->addColumn("netStakeStandard", "decimal")->addColumn("netStakeHighRoller", "decimal")
			->addColumn("netStakeCombined", "decimal")->addColumn("grossStakeStandard", "decimal")
			->addColumn("grossStakeHighRoller", "decimal")->addColumn("grossStakeCombined", "decimal")
			->create();
	}

	public function down(){
    	$this->table("Report")->drop();
	}
}
