<?php


use Phinx\Migration\AbstractMigration;

class TrackTable extends AbstractMigration
{
    public function up():void {
    	$track = $this->table("Track");
    	$track->addColumn("name","string")->addColumn("alternativeNames","json")->create();
	}

	public function down():void{
    	$this->table("Track")->drop();
	}
}
