<?php
namespace XpressBet\Repository\Spreadsheet;

use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use XpressBet\Repository\Spreadsheet\Interfaces\Bet365RepositoryInterface;
use XpressBet\Repository\Spreadsheet\Exception\InvalidPageNumberException;

class Bet365Repository implements Bet365RepositoryInterface {

	private $fileLocation;
	private $spreadsheet;

	function __construct($fileLocation){
    	$this->fileLocation = $fileLocation;

		$reader = IOFactory::createReaderForFile($this->fileLocation);
		$this->spreadsheet = $reader->load($this->fileLocation);
    }

    public function getPage($pageNum = 1){
		if($pageNum < 1){
			throw new InvalidPageNumberException("Must passed a page number greater than 1, number passed: {$pageNum}");
		}

		try {
			$sheet = $this->spreadsheet->getSheet($pageNum - 1);
			return $sheet->toArray();
		}catch (Exception $exception){}
	}

}