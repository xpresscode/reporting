<?php
namespace XpressBet\Commands;

use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use XpressBet\Service\Bet365Service;

class Bet365Command {

	private $bet365Service;

	/**
	 * Bet365Command constructor.
	 */
	public function __construct(Bet365Service $bet365Service)
	{
		$this->bet365Service = $bet365Service;
		$reader = IOFactory::createReaderForFile(__DIR__ . "../testFiles/May Report - Bet365.xlsx");
		$spreadsheet = $reader->load(__DIR__ . "../testFiles/May Report - Bet365.xlsx");

		$secondSheetIndex = $spreadsheet->getFirstSheetIndex()+1;

		try {
			$sheet = $spreadsheet->getSheet($secondSheetIndex);
			$sheet->toArray();


		} catch (Exception $e) {
		}

	}
}