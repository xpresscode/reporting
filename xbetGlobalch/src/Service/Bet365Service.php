<?php

namespace XpressBet\Service;


use DateTime;
use GuzzleHttp\Client;
use XpressBet\Repository\Spreadsheet\Bet365Repository;
use XpressBet\Repository\Spreadsheet\Exception\InvalidPageNumberException;
use XpressBet\Repository\Spreadsheet\Interfaces\Bet365RepositoryInterface;

class Bet365Service {

	private $bet365Repository;
	private $fileLocation;

	public function __construct($fileLocation){
		$this->bet365Repository = new Bet365Repository($fileLocation);
	}

	public function exportToTable() {

		$customer = "Bet 365";
		$table = "<table>
			<tr>
				<td>Customer | </td>
				<td>Year | </td>
				<td>Month | </td>
				<td>Day | </td>
				<td>Track | </td>
				<td>Race # | </td>
				<td>Post Time | </td>
				<td>Handle | </td>
				<td>Currency | </td>
				<td>Converted USD Amount</td>
			</tr>";

		$date = new DateTime();
		$exchangeRate = NULL;
		try {
			foreach ($this->bet365Repository->getPage(2) AS $id => $row) {
				if($id < 5){
					continue;
				}

				if(!empty($row[1])){
					$date = new DateTime($row[1]);
					$client = new Client([ "base_uri" => "https://api.exchangeratesapi.io" ]);
					$response = $client->request("GET", $date->format("Y-m-d"), [ "query" => [ "base" => "GBP" ] ]);

					$exchangeRate = json_decode($response->getBody()->getContents())->rates->USD;
				}

				if(empty($row[2])){
					continue;
				}

				$raceInfo = explode(" ", $row[2]);
				$raceNum = $raceInfo[1];
				$track = implode(" ", array_slice($raceInfo, 2));

				$handle = str_replace(",", "", substr($row[4], 2));
				$convertedAmount = round($handle * $exchangeRate, 2);

				$table .= "
				<tr>
					<td>{$customer}</td>
					<td>{$date->format("Y")}</td>
					<td>{$date->format("m")}</td>
					<td>{$date->format("d")}</td>
					<td>{$track}</td>
					<td>{$raceNum}</td>
					<td>Post Time</td>
					<td>{$handle}</td>
					<td>GBP</td>
					<td>{$convertedAmount}</td>
				</tr>";
			}
		} catch (InvalidPageNumberException $e) {
			$table .= "<tr>No Date to show</tr>";
		}
		$table .= "</table>";

		return $table;
	}

}