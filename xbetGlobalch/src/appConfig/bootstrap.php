<?php
/**
 * Created by PhpStorm.
 * User: edwardrussell
 * Date: 10/22/18
 * Time: 3:33 PM
 */

require_once __DIR__ . "../vendor/autoload.php";

use DI\ContainerBuilder;

$containerBuilder = new ContainerBuilder();
$containerBuilder->addDefinitions(__DIR__ . '/config.php');
$container = $containerBuilder->build();
return $container;