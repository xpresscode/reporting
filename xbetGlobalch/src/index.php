<?php

use XpressBet\Service\Bet365Service;

require_once 'vendor/autoload.php';

try{
	$bet365Service = new Bet365Service(__DIR__ . "/testFiles/May Report - Bet365.xlsx");
	echo $bet365Service->exportToTable();
}catch (Exception $exception){
	echo $exception->getMessage();
}